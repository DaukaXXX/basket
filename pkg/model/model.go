package model

import "go.mongodb.org/mongo-driver/bson/primitive"

type JWT struct {
	Token string				`json:"token"`
}

type Error struct {
	Message string 				`json:"message"`
}

type Basket struct {
	ID primitive.ObjectID  		`json:"id" bson:"_id"`
	Products []Product			`json:"products" bson:"products"`
}

type Product struct {
	Name string 				`json:"name" bson:"name"`
	Price int 					`json:"price" bson:"price"`
	Description string			`json:"desc" bson:"desc"`
}

func (p *Product) Equals(prod Product) bool {
	if p.Name == prod.Name && p.Description == prod.Description && p.Price == prod.Price{
		return true
	}
	return false
}


//type ProductForMongo struct {
//	Products []Product  		`json:"products" bson:"products"`
//}