package mongo_rep

import (
	"context"
	"dauka_basket/pkg/model"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)





type Basket struct {
	MyClient 		*mongo.Client
}





func (p *Basket) AddProduct(prod model.Product, idd primitive.ObjectID) (bool, error) {
	collection := p.MyClient.Database("basket_api").Collection("basket")
	ctx, _ := context.WithTimeout(context.Background(), 60*time.Second)


	err := collection.FindOne( ctx, bson.M{"_id": idd} )

	if err.Err() == mongo.ErrNoDocuments{
		newBasker := model.Basket{
			ID:          idd,
			Products: []model.Product{prod},
		}

		_ , err := collection.InsertOne(ctx, newBasker)
		if err != nil{
			return false, err
		}else {
			return true, nil
		}

	}else{
		var basket model.Basket
		err11 := err.Decode(&basket)
		if err11 != nil{
			return false, err11
		}
		//no
		basket.Products = append(basket.Products, prod)

		update := bson.M{"$set": bson.M{"products": basket.Products}}
		_, err3 := collection.UpdateOne(
			ctx,
			bson.M{"_id": idd},
			update,
		)
		if err3 != nil {
			return false, err3
		}else{
			return true, nil
		}

	}

}




func (p *Basket) DeleteProduct(prod model.Product, idd primitive.ObjectID)  ( error) {
	collection := p.MyClient.Database("basket_api").Collection("basket")
	ctx, _ := context.WithTimeout(context.Background(), 60*time.Second)


	err := collection.FindOne( ctx, bson.M{"_id": idd} )

	if err.Err() == mongo.ErrNoDocuments{
		return  errors.New("You have not added any product to your busket")
	}else{
		var basket model.Basket
		err11 := err.Decode(&basket)
		if err11 != nil{
			return  err11
		}

		id := -1
		hasOrNot := false

		for i, v := range basket.Products{
			if v.Equals(prod){
				hasOrNot = true
				id = i
			}
		}
		fmt.Println(hasOrNot)
		if hasOrNot {


			fmt.Println(basket.Products)
			basket.Products[id] = basket.Products[len(basket.Products)-1]
			basket.Products[len(basket.Products)-1] = model.Product{}
			basket.Products = basket.Products[:len(basket.Products)-1]

			fmt.Println(basket.Products)

			update := bson.M{"$set": bson.M{"products": basket.Products}}
			_, err3 := collection.UpdateOne(
				ctx,
				bson.M{"_id": idd},
				update,
			)
			if err3 != nil {
				return err3
			}else{
				return nil
			}

		}else {
			return errors.New("you have not such product in your basket")

		}


	}
}





func (p *Basket) BasketCheckout( idd primitive.ObjectID)  ( int, error) {
	collection := p.MyClient.Database("basket_api").Collection("basket")
	ctx, _ := context.WithTimeout(context.Background(), 60*time.Second)


	err := collection.FindOne( ctx, bson.M{"_id": idd} )

	if err.Err() == mongo.ErrNoDocuments{
		return  -1, errors.New("You have not added any product to your busket")
	}else{
		var basket model.Basket
		err11 := err.Decode(&basket)
		if err11 != nil{
			return -1, err11
		}

		if len(basket.Products) == 0{
			return  -1, errors.New("You have not added any product to your busket")
		}else{
			sum := 0
			for _, v := range basket.Products{
				sum+= v.Price
			}

			return sum, nil
		}
	}
}






type Products struct {
	MyClient 		*mongo.Client
}

//
//func (p *Products) GetProducts() (*model.ProductForMongo, error) {
//
//	var myres []model.ProductForMongo
//
//	collection := p.MyClient.Database("basket_api").Collection("products")
//	ctx, _ := context.WithTimeout(context.Background(), 60*time.Second)
//
//	results, err := collection.Find(ctx, bson.D{{}})
//
//	if err != nil {
//		return nil, err
//	}
//	err = results.Decode(&myres)
//	if err != nil {
//		return nil, err
//	}
//
//	return &myres[0], nil
//}
//
//
//
//
//
//func (p *Products) SetProducts()  {
//	collection := p.MyClient.Database("basket_api").Collection("products")
//	ctx, _ := context.WithTimeout(context.Background(), 60*time.Second)
//
//	prods := dataInit()
//	mongoo := model.ProductForMongo{Products: *prods}
//
//	collection.InsertOne(ctx, mongoo)
//}
//
//
//
//
//func dataInit() *[]model.Product {
//	 prods := []model.Product{}
//	 prods = append(prods, model.Product{
//		 Name:        "Iphone 12",
//		 Price:       500000,
//		 Description: "good mobile phone ",
//	 })
//
//	prods = append(prods, model.Product{
//		Name:        "MacBook Pro",
//		Price:       800000,
//		Description: "pro mac",
//	})
//	prods = append(prods, model.Product{
//		Name:        "MacBookd Air",
//		Price:       6000000,
//		Description: "good air mac ",
//	})
//	prods = append(prods, model.Product{
//		Name:        "Ipad",
//		Price:       4000000,
//		Description: "good mobile IPAD ",
//	})
//	prods = append(prods, model.Product{
//		Name:        "Airpods",
//		Price:       100000,
//		Description: "good wireless headphones ",
//	})
//
//	return &prods
//}