package main

import (
	"context"
	"dauka_basket/pkg/model/mongo_rep"
	"flag"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"net/http"
	"os"
	"time"
)

type application struct {
	errorLog 			*log.Logger
	infoLog 			*log.Logger
	products 			*mongo_rep.Products
	basket 				*mongo_rep.Basket
}



func (app *application) routes() http.Handler {
	mux := http.NewServeMux()
	mux.HandleFunc("/add", TokenVerifyMiddleWare( app.addProduct))
	mux.HandleFunc("/delete",TokenVerifyMiddleWare(  app.deleteProduct))
	//mux.HandleFunc("/product", TokenVerifyMiddleWare(app.getProducts))
	mux.HandleFunc("/checkout", TokenVerifyMiddleWare(app.checkout))

	return mux
}

func main() {
	addr := flag.String("addr", ":4001", "HTTP network address")
	flag.Parse()

	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	clientOptions := options.Client().ApplyURI("mongodb://mongofinal:27017")
	client1, _ := mongo.Connect(ctx, clientOptions)


	app := &application{
		errorLog: errorLog,
		infoLog: infoLog,
		products: &mongo_rep.Products{client1},
		basket: &mongo_rep.Basket{MyClient: client1},
	}

	//app.products.SetProducts()



	srv := &http.Server{
		Addr: *addr,
		ErrorLog: errorLog,
		Handler: app.routes(),
		IdleTimeout: time.Minute,
		ReadTimeout: 5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}



	infoLog.Printf("Starting server on %s", *addr)
	err := srv.ListenAndServe()
	errorLog.Fatal(err)
}