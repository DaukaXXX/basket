package main

import (
	"context"
	"dauka_basket/pkg/model"
	email_notify "dauka_basket/protos"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"google.golang.org/grpc"
	"log"
	"net/http"
	"strconv"
	"strings"
)

func (app *application) addProduct(w http.ResponseWriter, r *http.Request)  {
	claims, _ := extractClaims(r)
	id, _ := claims["user_id"].(primitive.ObjectID)


	var Product model.Product
	var error1 model.Error

	err := json.NewDecoder(r.Body).Decode(&Product)
	if err != nil {
		error1.Message = err.Error()
		RespondWithError(w, http.StatusBadRequest, error1)
	}

	_, err = app.basket.AddProduct(Product, id)
	if err != nil {
		error1.Message = err.Error()
		RespondWithError(w, http.StatusBadRequest, error1)
	}else {
		ResponseJSON(w, "you have successfully added product")
	}
}


func (app *application) deleteProduct(w http.ResponseWriter, r *http.Request)  {
	claims, _ := extractClaims(r)
	id, _ := claims["user_id"].(primitive.ObjectID)

	var Product model.Product
	var error1 model.Error

	err := json.NewDecoder(r.Body).Decode(&Product)
	if err != nil {
		error1.Message = err.Error()
		RespondWithError(w, http.StatusBadRequest, error1)
	}

	err = app.basket.DeleteProduct(Product, id)
	if err != nil {
		error1.Message = err.Error()
		RespondWithError(w, http.StatusBadRequest, error1)
	}else {
		ResponseJSON(w, "you have successfully deleted product")
	}

}


//func (app *application) getProducts(w http.ResponseWriter, r *http.Request)  {
//	prods1, err := app.products.GetProducts()
//	var error1 model.Error
//
//	if err != nil {
//		error1.Message = err.Error()
//		RespondWithError(w, http.StatusBadRequest, error1)
//	}
//
//	ResponseJSON(w, prods1)
//}

func ResponseJSON(w http.ResponseWriter, data interface{}) {
	json.NewEncoder(w).Encode(data)
}


func (app *application) checkout(w http.ResponseWriter, r *http.Request)  {

	claims, _ := extractClaims(r)

	id, _ := claims["user_id"].(primitive.ObjectID)

	var error1 model.Error


	sum ,err := app.basket.BasketCheckout(id)
	if err != nil {
		error1.Message = err.Error()
		RespondWithError(w, http.StatusBadRequest, error1)
	}else {
		fmt.Println(sum)
		srr:= "you have successfully ordered your products for " + strconv.Itoa(sum) + "$"
		fmt.Println(srr)


		conn, err := grpc.Dial("notifyapi:50051", grpc.WithInsecure())
		if err != nil {
			log.Fatalf("could not connect: %v", err)
		}
		defer conn.Close()

		c := email_notify.NewNotificationServiceClient(conn)

		doLongGreet(c, "d.zhaparov@astanait.edu.kz", srr)



		ResponseJSON(w, srr)

	}

}



func extractClaims(r *http.Request) (jwt.MapClaims, bool) {
	authHeader := r.Header.Get("Authorization")
	bearerToken := strings.Split(authHeader, " ")
	tokenStr := bearerToken[1]

	hmacSecretString := "hard_work_pays_off"
	hmacSecret := []byte(hmacSecretString)
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {

		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("There was an error")
		}
		return hmacSecret, nil
	})

	if err != nil {
		return nil, false
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {

		return claims, true
	} else {
		log.Printf("Invalid JWT Token")
		return nil, false
	}
}




func doLongGreet(c email_notify.NotificationServiceClient, email, srr string) {

	requests := email_notify.NotificationRequest{To: email, Sum: srr}

	ctx := context.Background()
	res, err := c.SingleNotify(ctx, &requests)
	if err != nil {
		log.Fatalf("error while calling LongGreet: %v", err)
	}

	if res.IsDone{
		fmt.Println("works")
	}else{
		fmt.Println("not works")
	}
}
